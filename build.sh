#!/usr/bin/env bash
set -e

yarn install
yarn format
yarn build